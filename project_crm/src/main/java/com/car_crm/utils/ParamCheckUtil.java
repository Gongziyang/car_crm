package com.car_crm.utils;

import cn.hutool.Hutool;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 20:07
 * @file: ParamCheckUtil
 * @description: 星期五
 */
public class ParamCheckUtil {

    public static boolean checkString(String param){
        boolean flag = true;
        if(StrUtil.isBlank(param)){
            flag = false;
        }
        return flag;
    }

    public static boolean checkObject(Object... o){
        boolean flag = true;
        for(Object obj : o){
            if(ObjectUtil.isNull(o)){
                flag = false;
                break;
            }
        }
        return flag;
    }
}
