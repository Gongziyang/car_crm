package com.car_crm.utils;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 20:23
 * @file: Predict
 * @description: 星期二
 */
public class Predict {


    public static void main(String[] args) {
        Double[][] arrPoints = {
                {1.0, 2.0, 3.0, 5.0},
                {2.0, 4.0, 6.0, 10.0}
        };

/** 预测 w为1 b为0*/

        System.out.println(predict(arrPoints)[0]);

        System.out.println(predict(arrPoints)[1]);

/**测试结果与预测相同*/
    }

    public static double[] predict(Double[][] points) {
        // y = w * X + b
        //result[0] = w , result[1] = b
        double[] result = new double[2];

        //x求和
        double xSum = 0;

        //x平均值
        double xAvg = 0;
        /**
         *  x {1.0, 2.0, 3.0, 5.0},
         *  y {2.0, 4.0, 6.0, 10.0}
         * **/
        for (int i = 0; i < points[0].length; i++) {
            xSum = xSum + points[0][i];
        }

        //求x平均
        xAvg = xSum / points[0].length;

        //w 的 上半部分
        double wTop = 0;
        for (int i = 0; i < points[0].length; i++) {
            wTop = wTop + (points[0][i] - xAvg) * points[1][i];
        }

        //Xi的平方累加
        double wDownPre = 0;

        //X和的平方除以点的个数
        double wDownLast = 0;
        xSum = 0;
        for (int i = 0; i < points[0].length; i++) {

            //求下半部分 左边的 那一块 ： Xi的平方的和
            wDownPre = wDownPre + points[0][i] * points[0][i];
            xSum = xSum + points[0][i];
        }

        //求下半部分 右边的那一块 ： Xi的和的平方  除以  坐标点个数
        wDownLast = (xSum * xSum) / points[0].length;

        //求下半部分的值
        double wDown = wDownPre - wDownLast;

        //求w的值
        double w = wTop / wDown;
        result[0] = w;
        double b = 0;
        for (int i = 0; i < points[0].length; i++) {

            //求 Yi - w * Xi 的累加和的值
            b = b + (points[1][i] - w * points[0][i]);
        }
        
        //求 b 的值
        b = b / points[0].length;
        result[1] = b;
        return result;
    }

}
