package com.car_crm.service;

import com.car_crm.vo.BaseResp;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqContract;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Contract;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:27
 * @file: ContractService
 * @description: 星期五
 */
public interface ContractService {

    BaseResp<Integer> insert(Contract contract);

    BaseResp<Integer> delete(int id);

    BaseResp<List<Contract>> getByPageHelper(ReqContract req);

    BaseResp<Integer> update(Contract contract);
}
