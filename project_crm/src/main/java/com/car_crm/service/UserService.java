package com.car_crm.service;

import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.PageBean;
import com.car_crm.vo.Req.ReqModify;
import com.car_crm.vo.common.User;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:54
 * @file: LoginService
 * @description: 星期五
 */
public interface UserService {

    User getUserByName(String userName);

    int insert(User user);

    /**
     * 获取所有用户列表，仅超级管理员拥有
     * **/
    BaseResp<List<User>> getAllUser(BaseReq<PageBean> req);

    BaseResp<Integer> modifyRole(ReqModify reqModify);

    BaseResp<Integer> delete(Integer userId);

    BaseResp<Integer> freezeUser(Integer userId);

    BaseResp<Integer> deFreezeUser(Integer userId);
}
