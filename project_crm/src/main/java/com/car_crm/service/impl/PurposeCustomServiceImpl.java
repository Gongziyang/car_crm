package com.car_crm.service.impl;

import com.car_crm.mapper.PurposeCustomMapper;
import com.car_crm.service.PurposeCustomService;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqPurposeCustom;
import com.car_crm.vo.common.PurposeCustom;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:43
 * @file: PurposeCustomServiceImpl
 * @description: 星期五
 */
@Service
public class PurposeCustomServiceImpl implements PurposeCustomService {

    @Autowired
    PurposeCustomMapper purposeCustomMapper;

    @Override
    public BaseResp<Integer> insert(PurposeCustom purposeCustom) {
        BaseResp<Integer> response = new BaseResp<>();
        int result = purposeCustomMapper.insert(purposeCustom);
        if(result == 0){
            response.setData(null);
            response.setMsg(ErrorCustomizer.INSERT_ERROR.getMsg());
            return response;
        }
        response.setMsg(ErrorCustomizer.INSERT_SUCCESS.getMsg());
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<Integer> delete(int id) {
        BaseResp<Integer> response = new BaseResp<>();
        int result = purposeCustomMapper.delete(id);
        if(result == 0){
            response.setData(null);
            response.setMsg(ErrorCustomizer.DELETE_ERROR.getMsg());
            return response;
        }
        response.setMsg(ErrorCustomizer.DELETE_SUCCESS.getMsg());
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<List<PurposeCustom>> getByPageHelper(ReqPurposeCustom req) {
        BaseResp<List<PurposeCustom>> response = new BaseResp<>();
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize==0){
            pageSize = 5;
        }
        PageHelper.startPage(pageNo,pageSize);
        List<PurposeCustom> byPageHelpler = purposeCustomMapper.getByPageHelpler(req);
        int itemSize = purposeCustomMapper.itemSize(req);
        if(itemSize == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        response.setData(byPageHelpler);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> update(PurposeCustom purposeCustom) {
        int result = purposeCustomMapper.update(purposeCustom);
        BaseResp<Integer> resp = new BaseResp<>();
        if(result == 0){
            resp.setMsg(ErrorCustomizer.UPDATE_ERROR.getMsg());
            resp.setData(null);
            return resp;
        }
        resp.setData(result);
        resp.setMsg(ErrorCustomizer.UPDATE_SUCCESS.getMsg());
        return resp;
    }
}
