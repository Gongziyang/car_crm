package com.car_crm.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.car_crm.mapper.BargainCustomMapper;
import com.car_crm.service.BargainCustomService;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.common.BargainCustom;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:41
 * @file: BargainCustomServiceImpl
 * @description: 星期五
 */
@Service
public class BargainCustomServiceImpl implements BargainCustomService {

    @Autowired
    BargainCustomMapper bargainCustomMapper;

    @Override
    public BaseResp<Integer> insert(BargainCustom bargainCustom) {
        int result = bargainCustomMapper.insert(bargainCustom);
        BaseResp<Integer> response = new BaseResp<>();
        if (result == 0) {
            response.setData(null);
            response.setMsg(ErrorCustomizer.INSERT_ERROR.getMsg());
            return response;
        }
        response.setData(result);
        response.setMsg(ErrorCustomizer.INSERT_SUCCESS.getMsg());
        return response;
    }

    @Override
    public BaseResp<Integer> delete(int id) {
        int result = bargainCustomMapper.delete(id);
        BaseResp<Integer> response = new BaseResp<>();
        if (result == 0) {
            response.setMsg(ErrorCustomizer.DELETE_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        response.setData(result);
        response.setMsg(ErrorCustomizer.DELETE_SUCCESS.getMsg());
        return response;
    }

    @Override
    public BaseResp<List<BargainCustom>> getByPageHelper(ReqBargainCustom req) {

        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize==0){
            pageSize = 5;
        }
        System.out.println("req:{}"+JSONObject.toJSONString(req));
        PageHelper.startPage(pageNo,pageSize);
        List<BargainCustom> byPageHelpler = bargainCustomMapper.getByPageHelpler(req);
        BaseResp<List<BargainCustom>> response = new BaseResp<>();
        if(byPageHelpler.size() == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int itemSize = bargainCustomMapper.itemSize(req);
        if(itemSize == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        response.setData(byPageHelpler);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> update(BargainCustom bargainCustom) {
        int result = bargainCustomMapper.update(bargainCustom);
        BaseResp<Integer> resp = new BaseResp<>();
        if(result == 0){
          resp.setMsg(ErrorCustomizer.UPDATE_ERROR.getMsg());
          resp.setData(null);
          return resp;
        }
        resp.setData(result);
        resp.setMsg(ErrorCustomizer.UPDATE_SUCCESS.getMsg());
        return resp;
    }

    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(new Date(System.currentTimeMillis()));

    }
}
