package com.car_crm.service.impl;

import com.car_crm.mapper.ContractMapper;
import com.car_crm.service.ContractService;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqContract;
import com.car_crm.vo.common.Contract;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:42
 * @file: ContractServiceImpl
 * @description: 星期五
 */
@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    ContractMapper contractMapper;

    @Override
    public BaseResp<Integer> insert(Contract contract) {
        BaseResp<Integer> response = new BaseResp<>();
        int result = contractMapper.insert(contract);
        if (result == 0) {
            response.setMsg(ErrorCustomizer.INSERT_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        response.setMsg(ErrorCustomizer.INSERT_SUCCESS.getMsg());
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<Integer> delete(int id) {
        BaseResp<Integer> response = new BaseResp<>();
        int result = contractMapper.delete(id);
        if (result == 0) {
            response.setData(null);
            response.setMsg(ErrorCustomizer.DELETE_ERROR.getMsg());
            return response;
        }
        response.setMsg(ErrorCustomizer.DELETE_SUCCESS.getMsg());
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<List<Contract>> getByPageHelper(ReqContract req) {
        BaseResp<List<Contract>> response = new BaseResp<>();
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize==0){
            pageSize = 5;
        }
        PageHelper.startPage(pageNo,pageSize);
        List<Contract> byPageHelpler = contractMapper.getByPageHelpler(req);
        int itemSize = contractMapper.itemSize(req);
        if(itemSize == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        response.setData(byPageHelpler);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> update(Contract contract) {
        int result = contractMapper.update(contract);
        BaseResp<Integer> resp = new BaseResp<>();
        if(result == 0){
            resp.setMsg(ErrorCustomizer.UPDATE_ERROR.getMsg());
            resp.setData(null);
            return resp;
        }
        resp.setData(result);
        resp.setMsg(ErrorCustomizer.UPDATE_SUCCESS.getMsg());
        return resp;
    }
}
