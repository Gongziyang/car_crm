package com.car_crm.service.impl;

import com.car_crm.mapper.ComplaintMapper;
import com.car_crm.service.ComplaintService;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqComplaint;
import com.car_crm.vo.common.Complaint;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:42
 * @file: ComplaintServiceImpl
 * @description: 星期五
 */
@Service
public class ComplaintServiceImpl implements ComplaintService {

    @Autowired
    ComplaintMapper complaintMapper;

    @Override
    public BaseResp<Integer> insert(Complaint complaint) {
        int result = complaintMapper.insert(complaint);
        BaseResp<Integer> response = new BaseResp<>();
        if (result == 0) {
            response.setMsg(ErrorCustomizer.INSERT_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        response.setData(result);
        response.setMsg(ErrorCustomizer.INSERT_SUCCESS.getMsg());
        return response;
    }

    @Override
    public BaseResp<Integer> delete(int id) {
        int result = complaintMapper.delete(id);
        BaseResp<Integer> response = new BaseResp<>();
        if (result == 0) {
            response.setMsg(ErrorCustomizer.DELETE_ERROR.getMsg());
            response.setData(null);
        }
        response.setData(result);
        response.setMsg(ErrorCustomizer.DELETE_SUCCESS.getMsg());
        return response;
    }

    @Override
    public BaseResp<List<Complaint>> getByPageHelper(ReqComplaint req) {
        BaseResp<List<Complaint>> response = new BaseResp<>();
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize==0){
            pageSize = 5;
        }
        PageHelper.startPage(pageNo,pageSize);
        List<Complaint> byPageHelpler = complaintMapper.getByPageHelpler(req);
        int itemSize = complaintMapper.itemSize(req);
        if(itemSize == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        response.setData(byPageHelpler);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> update(Complaint complaint) {
        int result = complaintMapper.update(complaint);
        BaseResp<Integer> resp = new BaseResp<>();
        if(result == 0){
            resp.setMsg(ErrorCustomizer.UPDATE_ERROR.getMsg());
            resp.setData(null);
            return resp;
        }
        resp.setData(result);
        resp.setMsg(ErrorCustomizer.UPDATE_SUCCESS.getMsg());
        return resp;
    }
}
