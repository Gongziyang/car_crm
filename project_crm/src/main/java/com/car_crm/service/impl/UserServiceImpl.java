package com.car_crm.service.impl;

import com.car_crm.mapper.UserMapper;
import com.car_crm.service.UserService;
import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.PageBean;
import com.car_crm.vo.Req.ReqModify;
import com.car_crm.vo.common.User;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:54
 * @file: LoginServiceImpl
 * @description: 星期五
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public User getUserByName(String userName) {
        return userMapper.selectUserByName(userName);
    }

    @Override
    public int insert(User user) {
        return userMapper.insert(user);
    }

    @Override
    public BaseResp<List<User>> getAllUser(BaseReq<PageBean> req) {
        int pageNo = req.getData().getPageNo();
        int pageSize = req.getData().getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize == 0){
            pageSize = 5;
        }
        int itemSize = userMapper.itemSize();
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        PageHelper.startPage(pageNo,pageSize);
        List<User> allUser = userMapper.getAllUser();
        BaseResp<List<User>> response = new BaseResp<>();
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setData(allUser);
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> modifyRole(ReqModify reqModify) {
        int i = userMapper.modifyRole(reqModify);
        BaseResp<Integer> response = new BaseResp<>();
        if(i == 0){
            response.setMsg("修改失败");
            response.setData(i);
            return  response;
        }
        response.setMsg("修改成功");
        response.setData(i);
        return response;
    }

    @Override
    public BaseResp<Integer> delete(Integer userId) {
        int result = userMapper.delete(userId);
        BaseResp<Integer> response = new BaseResp<>();
        if(result == 0){
            response.setMsg("删除失败");
            response.setData(result);
            return response;
        }
        response.setMsg("删除成功");
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<Integer> freezeUser(Integer userId) {
      int result = userMapper.freezeUser(userId);
        BaseResp<Integer> response = new BaseResp<>();
        if(result == 0){
            response.setMsg("冻结失败");
            response.setData(result);
            return response;
        }
        response.setMsg("冻结成功");
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<Integer> deFreezeUser(Integer userId) {
        int result = userMapper.deFreezeUser(userId);
        BaseResp<Integer> response = new BaseResp<>();
        if(result == 0){
            response.setMsg("解除冻结失败");
            response.setData(result);
            return response;
        }
        response.setMsg("解除冻结成功");
        response.setData(result);
        return response;
    }
}
