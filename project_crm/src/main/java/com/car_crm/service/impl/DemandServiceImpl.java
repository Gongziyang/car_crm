package com.car_crm.service.impl;

import com.car_crm.mapper.DemandMapper;
import com.car_crm.service.DemandService;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqDemand;
import com.car_crm.vo.common.Demand;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:42
 * @file: DemandServiceImpl
 * @description: 星期五
 */
@Service
public class DemandServiceImpl implements DemandService {

    @Autowired
    DemandMapper demandMapper;

    @Override
    public BaseResp<Integer> insert(Demand demand) {
        BaseResp<Integer> resposne = new BaseResp<>();
        int result = demandMapper.insert(demand);
        if (result == 0) {
            resposne.setData(null);
            resposne.setMsg(ErrorCustomizer.INSERT_ERROR.getMsg());
            return resposne;
        }
        resposne.setMsg(ErrorCustomizer.INSERT_SUCCESS.getMsg());
        resposne.setData(result);
        return resposne;
    }

    @Override
    public BaseResp<Integer> delete(int id) {
        BaseResp<Integer> response = new BaseResp<>();
        int result = demandMapper.delete(id);
        if (result == 0) {
            response.setData(null);
            response.setMsg(ErrorCustomizer.DELETE_ERROR.getMsg());
            return response;
        }
        response.setMsg(ErrorCustomizer.DELETE_SUCCESS.getMsg());
        response.setData(result);
        return response;
    }

    @Override
    public BaseResp<List<Demand>> getByPageHelper(ReqDemand req) {
        BaseResp<List<Demand>> response = new BaseResp<>();
        int pageNo = req.getPageNo();
        int pageSize = req.getPageSize();
        if(pageNo == 0){
            pageNo = 1;
        }
        if(pageSize==0){
            pageSize = 5;
        }
        PageHelper.startPage(pageNo,pageSize);
        List<Demand> byPageHelpler = demandMapper.getByPageHelpler(req);
        int itemSize = demandMapper.itemSize(req);
        if(itemSize == 0){
            response.setMsg(ErrorCustomizer.QUERY_ERROR.getMsg());
            response.setData(null);
            return response;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize/pageSize) : (itemSize/pageSize + 1);
        response.setData(byPageHelpler);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public BaseResp<Integer> update(Demand demand) {
        int result = demandMapper.update(demand);
        BaseResp<Integer> resp = new BaseResp<>();
        if(result == 0){
            resp.setMsg(ErrorCustomizer.UPDATE_ERROR.getMsg());
            resp.setData(null);
            return resp;
        }
        resp.setData(result);
        resp.setMsg(ErrorCustomizer.UPDATE_SUCCESS.getMsg());
        return resp;
    }
}
