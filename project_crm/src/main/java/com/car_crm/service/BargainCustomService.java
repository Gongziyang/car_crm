package com.car_crm.service;

import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.common.BargainCustom;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:27
 * @file: BargainCustomService
 * @description: 星期五
 */
public interface BargainCustomService {

    BaseResp<Integer> insert(BargainCustom bargainCustom);

    BaseResp<Integer> delete(int id);

    BaseResp<List<BargainCustom>> getByPageHelper(ReqBargainCustom req);

    BaseResp<Integer> update(BargainCustom bargainCustom);

}
