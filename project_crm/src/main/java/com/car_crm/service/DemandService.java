package com.car_crm.service;

import com.car_crm.vo.BaseResp;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqDemand;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Demand;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:27
 * @file: DemandService
 * @description: 星期五
 */
public interface DemandService {
    BaseResp<Integer> insert(Demand demand);

    BaseResp<Integer> delete(int id);

    BaseResp<List<Demand>> getByPageHelper(ReqDemand req);

    BaseResp<Integer> update(Demand demand);
}
