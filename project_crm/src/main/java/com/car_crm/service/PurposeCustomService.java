package com.car_crm.service;

import com.car_crm.vo.BaseResp;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqPurposeCustom;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.PurposeCustom;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:28
 * @file: PurposeCustomService
 * @description: 星期五
 */
public interface PurposeCustomService {

    BaseResp<Integer> insert(PurposeCustom purposeCustom);

    BaseResp<Integer> delete(int id);

    BaseResp<List<PurposeCustom>> getByPageHelper(ReqPurposeCustom req);

    BaseResp<Integer> update(PurposeCustom purposeCustom);
}
