package com.car_crm.service;

import com.car_crm.vo.BaseResp;
import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqComplaint;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:27
 * @file: ComplaintService
 * @description: 星期五
 */
public interface ComplaintService {

    BaseResp<Integer> insert(Complaint complaint);

    BaseResp<Integer> delete(int id);

    BaseResp<List<Complaint>> getByPageHelper(ReqComplaint req);

    BaseResp<Integer> update(Complaint complaint);
}
