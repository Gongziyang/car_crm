package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqPurposeCustom;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;
import com.car_crm.vo.common.PurposeCustom;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:12
 * @file: PurposeCustomMapper
 * @description: 星期五
 */
@Mapper
public interface PurposeCustomMapper {
    int insert(PurposeCustom purposeCustom);

    int delete(int id);

    List<PurposeCustom> getByPageHelpler(ReqPurposeCustom reqPurposeCustom);

    int itemSize(ReqPurposeCustom reqPurposeCustom);

    int update(PurposeCustom purposeCustom);

    List<PurposeCustom> selectAll();
}
