package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:11
 * @file: BargainCustomMapper
 * @description: 星期五
 */
@Mapper
public interface BargainCustomMapper {

    int insert(BargainCustom bargainCustom);

    int delete(int id);

    List<BargainCustom> getByPageHelpler(ReqBargainCustom reqBargainCustom);

    int itemSize(ReqBargainCustom reqBargainCustom);

    int update(BargainCustom bargainCustom);

    List<BargainCustom> selectAll();
}
