package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqContract;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;
import com.car_crm.vo.common.Contract;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:12
 * @file: ContractMapper
 * @description: 星期五
 */
@Mapper
public interface ContractMapper {
    int insert(Contract contract);

    int delete(int id);

    List<Contract> getByPageHelpler(ReqContract reqContract);

    int itemSize(ReqContract reqContract);

    int update(Contract contract);
}
