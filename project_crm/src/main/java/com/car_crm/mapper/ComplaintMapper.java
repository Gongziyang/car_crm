package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqComplaint;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:12
 * @file: ComplaintMapper
 * @description: 星期五
 */
@Mapper
public interface ComplaintMapper {

    int insert(Complaint complaint);

    int delete(int id);

    List<Complaint> getByPageHelpler(ReqComplaint reqComplaint);

    int itemSize(ReqComplaint reqComplaint);

    int update(Complaint complaint);
}
