package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqModify;
import com.car_crm.vo.common.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/30 13:59
 * @file: UserMapper
 * @description: 星期五
 */
@Mapper
public interface UserMapper {

    User selectUserByName(String userName);

    String getUserRoleById(Integer userId);

    int insert(User user);

    /**
     * 获取所有用户列表，仅超级管理员拥有
     * **/
    List<User> getAllUser();

    int modifyRole(ReqModify reqModify);

    int delete(Integer userId);

    int freezeUser(Integer userId);

    int deFreezeUser(Integer userId);

    int itemSize();
}
