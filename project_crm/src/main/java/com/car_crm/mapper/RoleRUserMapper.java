package com.car_crm.mapper;

import com.car_crm.vo.common.RoleRUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/19 19:35
 * @file: RoleRUserMapper
 * @description: 星期三
 */
@Mapper
public interface RoleRUserMapper {

    int insert(RoleRUser roleRUser);
}
