package com.car_crm.mapper;

import com.car_crm.vo.Req.ReqBargainCustom;
import com.car_crm.vo.Req.ReqDemand;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.Complaint;
import com.car_crm.vo.common.Demand;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:12
 * @file: DemandMapper
 * @description: 星期五
 */
@Mapper
public interface DemandMapper {
    int insert(Demand demand);

    int delete(int id);

    List<Demand> getByPageHelpler(ReqDemand reqDemand);

    int itemSize(ReqDemand reqDemand);

    int update(Demand demand);
}
