package com.car_crm.vo;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 19:02
 * @file: Constant
 * @description: 星期五
 */
public enum ErrorCustomizer {


    USERNAME_NOT_EXIST(0,"用户名不存在"),

    USERNAME_EXIST(0,"用户名已存在"),

    USERNAME_ERROR(-1,"用户名错误"),

    PASSWORD_ERROR(-2,"密码错误"),

    QUERY_ERROR(-3,"查询失败"),

    DELETE_ERROR(-4,"删除失败"),

    INSERT_ERROR(-5,"添加失败"),

    REGIST_ERROR(-6,"注册失败"),

    UPDATE_ERROR(6,"更新失败"),

    QUERY_SUCCESS(3,"查询成功"),

    DELETE_SUCCESS(4,"删除成功"),

    INSERT_SUCCESS(5,"插入成功"),

    UPDATE_SUCCESS(6,"更新成功"),

    REGIST_SUCCESS(7,"注册成功");

    private int code;

    private String msg;

    ErrorCustomizer(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
