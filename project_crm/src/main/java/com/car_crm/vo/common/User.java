package com.car_crm.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 11:02
 * @file: User
 * @description: 星期二
 */
@Data
public class User {

    private int id;

    private String userName;

    private String passWord;

    private String realName;

    private int flag;

    private int deleted;

    private String role;

}
