package com.car_crm.vo.common;

import com.car_crm.vo.PageBean;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:47
 * @file: BargainCustom
 * @description: 成交客户实体
 */
@Data
public class BargainCustom{

    /**
     * 主键,唯一标识
     * **/
    private int id;

    /**
     * 客户姓名
     * **/
    private String name;

    /**
     * 客户联系方式
     * **/
    private String tel;

    /**
     * 产品名称
     * **/
    private String productName;

    /**
     * 交易金额
     * **/
    private float tradingVolume;

    /**
     * 客户来源 0线下宣传 1网络营销 2其他
     * **/
    private int customSource;

    /**
     * 产品信息
     * **/
    private String productInfo;

    private Timestamp time;

    private  float income;
}
