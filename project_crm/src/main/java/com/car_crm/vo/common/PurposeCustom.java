package com.car_crm.vo.common;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:52
 * @file: PurposeCustom
 * @description: 意向客户实体
 */
@Data
public class PurposeCustom {

    /**
     * 主键，唯一标识
     * **/
    private int id;

    /**
     * 客户姓名
     * **/
    private String name;

    /**
     * 客户联系方式
     ***/
    private String tel;

    /**
     * 意向产品
     * **/
    private String purposeProduct;

    /**
     * 期望价格
     * **/
    private float purposePrice;

    /**
     * 客户来源
     * **/
    private int customSource;

    /**
     * 产品信息
     * **/
    private String productInfo;

    private float income;

}
