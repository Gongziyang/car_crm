package com.car_crm.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 23:38
 * @file: CustomStatInfo
 * @description: 星期二
 */
@Data
public class CustomStatInfo {

    private int totalItem;

    //朋友推荐
    private int friendRecommendation;

    //网络邀约
    private int networkInvitation;

    //现场考察
    private int siteInspection;

    //电话邀约
    private int telInvitation;

}
