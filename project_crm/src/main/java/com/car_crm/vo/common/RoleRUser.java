package com.car_crm.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/19 19:35
 * @file: RoleRUser
 * @description: 星期三
 */
@Data
public class RoleRUser {

    private int id;

    private int user_id;

    private int role_id;

}
