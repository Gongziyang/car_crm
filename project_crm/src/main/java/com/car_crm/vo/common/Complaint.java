package com.car_crm.vo.common;

import com.car_crm.vo.PageBean;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:58
 * @file: Complaint
 * @description: 客户投诉表
 */
@Data
public class Complaint {

    /**
     * 唯一标识
     **/
    private int id;

    /**
     * 联系方式
     **/
    private String tel;

    /**
     * 投诉时间
     **/
    private Timestamp time;

    /**
     * 是否反馈
     **/
    private int flag;

    /**
     * 投诉详情
     **/
    private String info;

    /**
     * 客户名称
     **/
    private String name;

}
