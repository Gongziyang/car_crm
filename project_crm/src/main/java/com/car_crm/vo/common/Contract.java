package com.car_crm.vo.common;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 15:00
 * @file: Contract
 * @description: 合同表
 */
@Data
public class Contract {

    /**
     * id，唯一标识
     * **/
    private int id;

    /**
     * 合同编号
     * **/
    private String number;

    /**
     * 客户名称
     * **/
    private String name;

    /**
     * 销售人员姓名
     * **/
    private String SalesName;

    /**
     * 支付方式
     * **/
    private int payType;

    /**
     * 交易金额
     * **/
    private float tradingVolume;

    /**
     * 产品详情
     * **/
    private String productInfo;

}
