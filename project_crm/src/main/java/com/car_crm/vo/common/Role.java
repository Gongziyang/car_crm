package com.car_crm.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 11:05
 * @file: Role
 * @description: 星期二
 */
@Data
public class Role {

    private int id;

    private String roleName;

    private String description;

}
