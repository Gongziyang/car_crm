package com.car_crm.vo.common;

import com.car_crm.vo.PageBean;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:56
 * @file: Demand
 * @description: 客户需求实体
 */
@Data
public class Demand {

    /**
     * 主键，唯一标识
     * **/
    private int id;

    /**
     * 产品名称
     * **/
    private String productName;

    /**
     * 产品型号
     * **/
    private int productType;

    /**
     * 保险类型
     * **/
    private int insuranceType;

    /**
     * 需求时间
     * **/
    private Timestamp demandTime;

    /**
     * 产品名称
     * **/
    private String productInfo;

}
