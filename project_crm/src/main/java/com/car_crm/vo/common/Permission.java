package com.car_crm.vo.common;

import lombok.Data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 11:10
 * @file: Permission
 * @description: 星期二
 */
@Data
public class Permission {

    private int id;

    private String name;

    private String description;

    private String url;

    public List<Menu> buildTree(List<Menu> menus) {
        //构建菜单树
        for (Menu m : menus){
           for(Menu mm : menus){
               if(mm.getPid() == m.getId()){
                   m.getChildren().add(mm);
               }
           }
        }
        //只保留根元素
        Iterator<Menu> iterator = menus.iterator();
        while(iterator.hasNext()){
            Menu m = iterator.next();
            if(m.getId()!=0){
                iterator.remove();
            }
        }
        return menus;
    }
}
