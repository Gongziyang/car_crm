package com.car_crm.vo.common;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/19 0:38
 * @file: BargainStat
 * @description: 星期三
 */
@Data
public class BargainStat {

    private int janaury;

    private int feburay;

    private int march;

    private int april;

    private int may;

    private int june;

    private int july;

    private int august;

    private int sebtember;

    private int octember;

    private int november;

    private int december;

}
