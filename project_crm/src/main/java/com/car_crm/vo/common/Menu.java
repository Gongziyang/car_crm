package com.car_crm.vo.common;

import lombok.Data;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 11:08
 * @file: Menu
 * @description: 星期二
 */
@Data
public class Menu {

    private int id;

    private String name;

    private int pid;

    private List<Menu> children;

}
