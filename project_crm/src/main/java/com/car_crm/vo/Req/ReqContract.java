package com.car_crm.vo.Req;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:24
 * @file: ReqContract
 * @description: 星期五
 */
@Data
public class ReqContract extends PageBean {

    /**
     * 合同编号
     **/
    private String number = null;

    /**
     * 客户名称
     * **/
    private String customerName = null;
}
