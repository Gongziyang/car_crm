package com.car_crm.vo.Req;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:21
 * @file: ReqDemand
 * @description: 星期五
 */
@Data
public class ReqDemand extends PageBean {

    /**
     * 产品名称
     * **/
    private String productName = null;

}
