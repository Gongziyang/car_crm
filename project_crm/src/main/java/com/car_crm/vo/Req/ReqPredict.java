package com.car_crm.vo.Req;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/19 17:00
 * @file: ReqPredict
 * @description: 星期三
 */
@Data
public class ReqPredict {

    private float income;

    private float purposePrice;

}
