package com.car_crm.vo.Req;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/4 9:53
 * @file: ReqModify
 * @description: 星期五
 */
@Data
public class ReqModify {

    private int userId;

    private int roleId;

}
