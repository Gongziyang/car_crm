package com.car_crm.vo.Req;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:19
 * @file: ReqPurposeCustom
 * @description: 星期五
 */
@Data
public class ReqPurposeCustom extends PageBean {

    /**
     * 客户姓名
     * **/
    private String name = null;

    /**
     * 意向产品
     * **/
    private String purposeProduct = null;

    /**
     * 客户来源
     * **/
    private int customSource = -1;

}
