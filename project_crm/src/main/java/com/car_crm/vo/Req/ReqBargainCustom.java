package com.car_crm.vo.Req;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:16
 * @file: ReqBargainCustom
 * @description: 查询成交客户信息实体
 */
@Data
public class ReqBargainCustom extends PageBean {

    /**
     * 客户姓名
     * **/
    private String name = null;

    /**
     * 产品名称
     * **/
    private String productName = null;

}
