package com.car_crm.vo.Req;

import com.car_crm.vo.PageBean;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 17:22
 * @file: ReqComplaint
 * @description: 星期五
 */
@Data
public class ReqComplaint extends PageBean {

    /**
     * 投诉信息
     * **/
    private String info;

}
