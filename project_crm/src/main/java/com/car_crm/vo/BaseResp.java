package com.car_crm.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:40
 * @file: BaseResponse
 * @description: 星期五
 */
@Data
public class BaseResp<T> {

    /**
     * 总条目数
     * **/
    @JsonProperty
    private int itemSize;

    /**
     * 总页数
     * **/
    @JsonProperty
    private int pageCount;

    /**
     * 响应数据
     * **/
    @JsonProperty
    private T Data;

    /**
     * 说明
     * **/
    @JsonProperty
    private String Msg;
}
