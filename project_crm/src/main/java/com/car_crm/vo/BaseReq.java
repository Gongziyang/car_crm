package com.car_crm.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:40
 * @file: BaseReq
 * @description: 星期五
 */
@Data
public class BaseReq<T> {

    @JsonProperty
    private String TimeStamp;
    @JsonProperty
    private T Data;
}
