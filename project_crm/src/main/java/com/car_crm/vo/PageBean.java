package com.car_crm.vo;

import lombok.Data;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 14:44
 * @file: PageBean
 * @description: 星期五
 */
@Data
public class PageBean {

    private int pageNo;

    private int pageSize;

}
