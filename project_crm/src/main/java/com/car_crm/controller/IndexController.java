package com.car_crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/30 13:24
 * @file: IndexController
 * @description: 星期五
 */
@Controller
public class IndexController{

    /**
     * 登录界面首页
     * **/
    @RequestMapping("login")
    public String login(){
        return "login";
    }

    /**
     * 欢迎界面
     * **/
    @RequestMapping("index")
    public String index(){
        return "index";
    }

    /**
     * 后端主界面
     * **/
    @RequestMapping("main")
    public String main(){
        return "HelloWord";
    }

}
