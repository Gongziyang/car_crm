package com.car_crm.controller;

import com.alibaba.fastjson.JSONObject;
import com.car_crm.mapper.BargainCustomMapper;
import com.car_crm.mapper.PurposeCustomMapper;
import com.car_crm.service.BargainCustomService;
import com.car_crm.service.PurposeCustomService;
import com.car_crm.utils.Predict;
import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.Req.ReqPredict;
import com.car_crm.vo.common.BargainCustom;
import com.car_crm.vo.common.BargainStat;
import com.car_crm.vo.common.CustomStatInfo;
import com.car_crm.vo.common.PurposeCustom;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/18 23:35
 * @file: StatController
 * @description: 统计预测接口
 */
@RestController
@RequestMapping("crc/")
@Api
public class StatController {

    @Autowired
    BargainCustomMapper bargainCustomMapper;

    @Autowired
    PurposeCustomMapper purposeCustomMapper;

    @ApiOperation(tags = "客户来源统计信息接口", value = "custom_stat_info")
    @RequestMapping(value = "custom_stat_info", method = RequestMethod.POST)
    public BaseResp<CustomStatInfo> getCustomStatInfo() {
        CustomStatInfo customStatInfo = new CustomStatInfo();
        List<BargainCustom> bargainCustoms = bargainCustomMapper.selectAll();
        List<PurposeCustom> purposeCustoms = purposeCustomMapper.selectAll();
        int totalSize = bargainCustoms.size() + purposeCustoms.size();
        customStatInfo.setTotalItem(totalSize);
        int networkInvitation = 0;
        int friendRecommendation = 0;
        int siteInspection = 0;
        int telInvitation = 0;
        for (BargainCustom b : bargainCustoms) {
            if (b.getCustomSource() == 0) {
                networkInvitation++;
            } else if (b.getCustomSource() == 1) {
                friendRecommendation++;
            } else if (b.getCustomSource() == 2) {
                siteInspection++;
            } else if (b.getCustomSource() == 3) {
                telInvitation++;
            }
        }
        for (PurposeCustom p : purposeCustoms) {
            if (p.getCustomSource() == 0) {
                networkInvitation++;
            } else if (p.getCustomSource() == 1) {
                friendRecommendation++;
            } else if (p.getCustomSource() == 2) {
                siteInspection++;
            } else if (p.getCustomSource() == 3) {
                telInvitation++;
            }
        }
        customStatInfo.setNetworkInvitation(networkInvitation);
        customStatInfo.setFriendRecommendation(friendRecommendation);
        customStatInfo.setSiteInspection(siteInspection);
        customStatInfo.setTelInvitation(telInvitation);
        BaseResp<CustomStatInfo> resp = new BaseResp<>();
        resp.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        resp.setData(customStatInfo);
        return resp;
    }

    @ApiOperation(tags = "成交趋势统计信息接口", value = "bargain_stat_info")
    @RequestMapping(value = "bargain_stat_info", method = RequestMethod.POST)
    public BaseResp<BargainStat> getBargainStatInfo() {
        int janaury = 0;
        int feburay = 0;
        int march = 0;
        int april = 0;
        int may = 0;
        int june = 0;
        int july = 0;
        int august = 0;
        int sebtember = 0;
        int octember = 0;
        int november = 0;
        int december = 0;
        List<BargainCustom> bargainCustoms = bargainCustomMapper.selectAll();

        System.out.println("统计数据："+ JSONObject.toJSONString(bargainCustoms));
        for (BargainCustom b : bargainCustoms) {
            //获取月份
            int month = b.getTime().toLocalDateTime().getMonth().getValue();
            switch (month) {
                case 1:
                    janaury++;
                    break;
                case 2:
                    feburay++;
                    break;
                case 3:
                    march++;
                    break;
                case 4:
                    april++;
                    break;
                case 5:
                    may++;
                    break;
                case 6:
                    june++;
                    break;
                case 7:
                    july++;
                    break;
                case 8:
                    august++;
                case 9:
                    sebtember++;
                    break;
                case 10:
                    octember++;
                    break;
                case 11:
                    november++;
                    break;
                case 12:
                    december++;
                    break;
                default:
                    break;
            }
        }
        BaseResp<BargainStat> resp = new BaseResp<>();
        BargainStat bargainStat = new BargainStat();
        bargainStat.setJanaury(janaury);
        bargainStat.setFeburay(feburay);
        bargainStat.setMarch(march);
        bargainStat.setApril(april);
        bargainStat.setMay(may);
        bargainStat.setJune(june);
        bargainStat.setJuly(july);
        bargainStat.setAugust(august);
        bargainStat.setSebtember(sebtember);
        bargainStat.setOctember(octember);
        bargainStat.setNovember(november);
        bargainStat.setDecember(december);
        resp.setData(bargainStat);
        resp.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        return resp;
    }


    @ApiOperation(tags = "预测成交概率信息接口", value = "predict_bargain_rate")
    @RequestMapping(value = "predict_bargain_rate", method = RequestMethod.POST)
    public BaseResp<String> predictBargainRate(@RequestBody() BaseReq<ReqPredict> req) {
        float purposeVolume = req.getData().getPurposePrice();
        float income = req.getData().getIncome();
        List<BargainCustom> bargainCustoms = bargainCustomMapper.selectAll();
        List<PurposeCustom> purposeCustoms = purposeCustomMapper.selectAll();
        //x坐标为客户收入，Y坐标为意向价格和成交价格
        Double data[][] = new Double[2][bargainCustoms.size() + purposeCustoms.size()];
        int index = 0;
        for (BargainCustom b : bargainCustoms) {
            //获取客户收入
            data[0][index] = (double) b.getIncome();
            //获取客户成交价
            data[1][index] = (double) b.getTradingVolume();
            index++;
        }
        for (PurposeCustom p : purposeCustoms) {
            data[0][index] = (double) p.getIncome();
            data[1][index] = (double) p.getPurposePrice();
            index++;
        }
        double[] linePara = Predict.predict(data);
        double w = linePara[0];
        double b = linePara[1];
        // y = income * w + b
        double predictResult = income * w + b;

        String resp = null;
        if (predictResult * 2 > purposeVolume) {
            resp = "大概率成交[成交率95%以上]";
        } else if (predictResult * 2 < purposeVolume) {
            if (predictResult * 2 / purposeVolume > 0.8) {
                resp = "非常有望成交[成交率80%以上]";
            }
            if (predictResult * 2 / purposeVolume > 0.6) {
                resp = "有希望成交[成交率60%]";
            }
            if (predictResult * 2 / purposeVolume > 0.5) {
                resp = "可能成交[成交率50%]";
            }
            if (predictResult * 2 / purposeVolume < 0.5) {
                resp = "成交机会较小[低于50%]";
            }
        }
        BaseResp<String> response = new BaseResp<>();
        response.setData(resp);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        return  response;
    }
}
