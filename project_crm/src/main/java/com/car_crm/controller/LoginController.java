package com.car_crm.controller;

import cn.hutool.core.util.ObjectUtil;
import com.car_crm.mapper.RoleRUserMapper;
import com.car_crm.service.UserService;
import com.car_crm.utils.ParamCheckUtil;
import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.ErrorCustomizer;
import com.car_crm.vo.common.RoleRUser;
import com.car_crm.vo.common.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:28
 * @file: LoginController
 * @description: 星期五
 */
@RestController
@CrossOrigin
@RequestMapping("crc/")
@Api()
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    RoleRUserMapper roleRUserMapper;

    @ApiOperation(tags = "登录接口",value = "crc/login")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "用户名",value = "username",dataType = "String",paramType = "String",required = true,example = "小李"),
            @ApiImplicitParam(name = "密码",value = "password",dataType = "String",paramType = "String",required = true,example = "1234456")
    })
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public BaseResp<User> login(@RequestBody() BaseReq<User> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        String username = req.getData().getUserName();
        String password = req.getData().getPassWord();
        User dbUser = userService.getUserByName(username);
        BaseResp<User> response = new BaseResp<>();
        if(ObjectUtil.isNull(dbUser)){
            response.setMsg(ErrorCustomizer.USERNAME_NOT_EXIST.getMsg());
            response.setData(null);
            return response;
        }
        if(!password.equals(dbUser.getPassWord())){
            response.setMsg("密码错误");
            response.setData(null);
            return response;
        }
        if(dbUser.getFlag() == -1){
            response.setMsg("账户已被冻结，请联系管理员");
            response.setData(null);
            return response;
        }
        response.setData(dbUser);
        response.setMsg(ErrorCustomizer.QUERY_SUCCESS.getMsg());
        return response;
    }


    @ApiOperation(tags = "注册接口",value = "crc/regist")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "用户名",value = "username",dataType = "String",paramType = "String",required = true,example = "小李"),
            @ApiImplicitParam(name = "密码",value = "password",dataType = "String",paramType = "String",required = true,example = "1234456")
    })
    @RequestMapping(value = "regist", method = RequestMethod.POST)
    public BaseResp<Integer> regist(@RequestBody() BaseReq<User> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        String username = req.getData().getUserName();
        User dbUser = userService.getUserByName(username);
        BaseResp<Integer> response = new BaseResp<>();
        if(!ObjectUtil.isNull(dbUser)){
            response.setMsg(ErrorCustomizer.USERNAME_EXIST.getMsg());
            response.setData(-1);
            return response;
        }
        int insert = userService.insert(req.getData());
        RoleRUser roleRUser = new RoleRUser();
        roleRUser.setRole_id(3);
        roleRUser.setUser_id(req.getData().getId());
        roleRUserMapper.insert(roleRUser);
        if(insert == 0){
            response.setMsg(ErrorCustomizer.REGIST_ERROR.getMsg());
            response.setData(-1);
            return response;
        }
        response.setData(insert);
        response.setMsg(ErrorCustomizer.REGIST_SUCCESS.getMsg());
        return response;
    }
}
