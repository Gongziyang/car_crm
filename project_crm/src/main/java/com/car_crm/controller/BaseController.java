package com.car_crm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.car_crm.mapper.UserMapper;
import com.car_crm.service.*;
import com.car_crm.utils.ParamCheckUtil;
import com.car_crm.vo.BaseReq;
import com.car_crm.vo.BaseResp;
import com.car_crm.vo.PageBean;
import com.car_crm.vo.Req.*;
import com.car_crm.vo.common.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/7 18:28
 * @file: BaseController
 * @description: 星期五
 */
@RestController
@RequestMapping("crc/")
@Api
public class BaseController {

    Logger logger = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    BargainCustomService bargainCustomService;

    @Autowired
    ComplaintService complaintService;

    @Autowired
    ContractService contractService;

    @Autowired
    PurposeCustomService purposeCustomService;

    @Autowired
    DemandService demandService;

    @Autowired
    UserService userService;

    @ApiOperation(tags = "查询已成交客户信息接口", value = "query_bargain_custom_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "客户名称", dataType = "string", paramType = "query", example = "张三"),
            @ApiImplicitParam(name = "productName", value = "产品名称", dataType = "string", paramType = "query", example = "沃尔沃C60")
    })
    @RequestMapping(value = "query_bargain_custom_info", method = RequestMethod.POST)
    public BaseResp<List<BargainCustom>> queryBargainInfo(@RequestBody() BaseReq<ReqBargainCustom> req) {
        logger.info("received a request:{}", JSONObject.toJSONString(req));
//        if (!ParamCheckUtil.checkObject(req, req.getData())) {
//            throw new RuntimeException("参数校验错误");
//        }
        return bargainCustomService.getByPageHelper(req.getData());
    }

    @ApiOperation(tags = "新增成交客户信息接口", value = "insert_bargain_custom_info")
    @RequestMapping(value = "insert_bargain_custom_info", method = RequestMethod.POST)
    public BaseResp<Integer> insertBargainInfo(@RequestBody() BaseReq<BargainCustom> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return bargainCustomService.insert(req.getData());
    }

    @ApiOperation(tags = "编辑成交客户信息接口", value = "update_bargain_custom_info")
    @RequestMapping(value = "update_bargain_custom_info", method = RequestMethod.POST)
    public BaseResp<Integer> updateBargainInfo(@RequestBody() BaseReq<BargainCustom> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return bargainCustomService.update(req.getData());
    }

    @ApiOperation(tags = "删除已成交客户信息接口", value = "delete_bargain_custom_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", dataType = "Integer", paramType = "delete", example = "1")
    })
    @RequestMapping(value = "delete_bargain_custom_info",method = RequestMethod.POST)
    public BaseResp<Integer> deleteBargainInfo(@RequestBody() BaseReq<Integer> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return bargainCustomService.delete(req.getData());
    }

    @ApiOperation(tags = "查询投诉信息接口", value = "query_complaint_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "info", value = "投诉信息", dataType = "string", paramType = "query", example = "汽车性能不好"),
    })
    @RequestMapping(value = "query_complaint_info", method = RequestMethod.POST)
    public BaseResp<List<Complaint>> queryComplaintInfo(@RequestBody() BaseReq<ReqComplaint> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return complaintService.getByPageHelper(req.getData());
    }

    @ApiOperation(tags = "新增投诉信息接口", value = "insert_complaint_info")
    @RequestMapping(value = "insert_complaint_info", method = RequestMethod.POST)
    public BaseResp<Integer> insertComplaintInfo(@RequestBody() BaseReq<Complaint> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return complaintService.insert(req.getData());
    }

    @ApiOperation(tags = "编辑投诉信息接口", value = "update_complaint_info")
    @RequestMapping(value = "update_complaint_info", method = RequestMethod.POST)
    public BaseResp<Integer> updateComplaintInfo(@RequestBody() BaseReq<Complaint> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return complaintService.update(req.getData());
    }

    @ApiOperation(tags = "删除投诉信息接口", value = "delete_complaint_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", dataType = "Integer", paramType = "delete", example = "1")
    })
    public BaseResp<Integer> deleteComplaintInfo(@RequestBody() BaseReq<Integer> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return complaintService.delete(req.getData());
    }

    @ApiOperation(tags = "查询合同信息接口", value = "query_contract_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "number", value = "合同编号", dataType = "string", paramType = "query", example = "SASD1205"),
            @ApiImplicitParam(name = "cutomerName", value = "客户姓名", dataType = "string", paramType = "query", example = "张三"),
    })
    @RequestMapping(value = "query_contract_info", method = RequestMethod.POST)
    public BaseResp<List<Contract>> queryContractInfo(@RequestBody() BaseReq<ReqContract> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return contractService.getByPageHelper(req.getData());
    }

    @ApiOperation(tags = "插入合同信息接口", value = "insert_contract_info")
    @RequestMapping(value = "insert_contract_info", method = RequestMethod.POST)
    public BaseResp<Integer> insertContractInfo(@RequestBody() BaseReq<Contract> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return contractService.insert(req.getData());
    }

    @ApiOperation(tags = "编辑合同信息接口", value = "update_contract_info")
    @RequestMapping(value = "update_contract_info", method = RequestMethod.POST)
    public BaseResp<Integer> updateContractInfo(@RequestBody() BaseReq<Contract> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return contractService.update(req.getData());
    }

    @ApiOperation(tags = "删除合同信息接口", value = "delete_contract_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", dataType = "Integer", paramType = "delete", example = "1")
    })
    @RequestMapping(value = "delete_contract_info",method = RequestMethod.POST)
    public BaseResp<Integer> deleteContractInfo(@RequestBody() BaseReq<Integer> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return contractService.delete(req.getData());
    }

    @ApiOperation(tags = "查询意向客户信息接口", value = "query_purpose_custom_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "客户姓名", dataType = "string", paramType = "query", example = "张三"),
            @ApiImplicitParam(name = "purposeProduct", value = "意向产品", dataType = "string", paramType = "query", example = "沃尔沃c60"),
            @ApiImplicitParam(name = "customSource", value = "客户来源", dataType = "string", paramType = "query", example = "网络营销"),
    })
    @RequestMapping(value = "query_purpose_custom_info", method = RequestMethod.POST)
    public BaseResp<List<PurposeCustom>> queryPurposeInfo(@RequestBody() BaseReq<ReqPurposeCustom> req) {
        logger.info("received a request:{}", JSONObject.toJSONString(req));
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return purposeCustomService.getByPageHelper(req.getData());
    }

    @ApiOperation(tags = "插入意向客户信息接口", value = "insert_purpose_custom_info")
    @RequestMapping(value = "insert_purpose_custom_info", method = RequestMethod.POST)
    public BaseResp<Integer> insertPurposeInfo(@RequestBody() BaseReq<PurposeCustom> req) {
        logger.info("received a request:{}", JSONObject.toJSONString(req));
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return purposeCustomService.insert(req.getData());
    }

    @ApiOperation(tags = "编辑意向客户信息接口", value = "update_purpose_custom_info")
    @RequestMapping(value = "update_purpose_custom_info", method = RequestMethod.POST)
    public BaseResp<Integer> updatePurposeCustomInfo(@RequestBody() BaseReq<PurposeCustom> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return purposeCustomService.update(req.getData());
    }

    @ApiOperation(tags = "删除意向客户信息接口", value = "delete_purpose_custom_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", dataType = "Integer", paramType = "delete", example = "1")
    })
    @RequestMapping(value = "delete_purpose_custom_info",method = RequestMethod.POST)
    public BaseResp<Integer> deletePurposeCustomInfo(@RequestBody() BaseReq<Integer> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return purposeCustomService.delete(req.getData());
    }

    @ApiOperation(tags = "查询客户需求信息接口", value = "query_demand_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productName", value = "产品名称", dataType = "string", paramType = "query", example = "沃尔沃C60"),
    })
    @RequestMapping(value = "query_demand_info", method = RequestMethod.POST)
    public BaseResp<List<Demand>> queryDemandInfo(@RequestBody() BaseReq<ReqDemand> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return demandService.getByPageHelper(req.getData());
    }

    @ApiOperation(tags = "插入客户需求信息接口", value = "insert_demand_info")
    @RequestMapping(value = "insert_demand_info", method = RequestMethod.POST)
    public BaseResp<Integer> insertDemandInfo(@RequestBody() BaseReq<Demand> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return demandService.insert(req.getData());
    }

    @ApiOperation(tags = "编辑客户需求信息接口", value = "update_demand_info")
    @RequestMapping(value = "update_demand_info", method = RequestMethod.POST)
    public BaseResp<Integer> updateDemandInfo(@RequestBody() BaseReq<Demand> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return demandService.update(req.getData());
    }

    @ApiOperation(tags = "删除客户需求信息接口", value = "delete_demand_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", dataType = "Integer", paramType = "delete", example = "1")
    })
    @RequestMapping(value = "delete_demand_info",method = RequestMethod.POST)
    public BaseResp<Integer> deleteDemandInfo(@RequestBody() BaseReq<Integer> req) {
        if (!ParamCheckUtil.checkObject(req, req.getData())) {
            throw new RuntimeException("参数校验错误");
        }
        return demandService.delete(req.getData());
    }

    @ApiOperation(tags = "查询用户信息接口", value = "query_user_info")
    @RequestMapping(value = "query_user_info",method = RequestMethod.POST)
    public BaseResp<List<User>> queryUsersInfo(@RequestBody()BaseReq<PageBean> req){
        BaseResp<List<User>> allUser = userService.getAllUser(req);
        return allUser;
    }

    @ApiOperation(tags = "修改用户权限接口", value = "modify_user_jurisdiction")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "Integer", paramType = "update", example = "1"),
            @ApiImplicitParam(name = "roleId", value = "角色ID  只能是 2 或 3 或 4 ", dataType = "Integer", paramType = "update", example = "4"),
    })
    @RequestMapping(value = "modify_user_jurisdiction",method = RequestMethod.POST)
    public BaseResp<Integer> modifyJurisdiction(@RequestBody() BaseReq<ReqModify> req){
        System.out.println("修改请求："+JSONObject.toJSONString(req.getData()));
        return userService.modifyRole(req.getData());
    }

    @ApiOperation(tags = "删除用户接口", value = "delete_user")
    @RequestMapping(value = "delete_user",method = RequestMethod.POST)
    public BaseResp<Integer> deleteUser(@RequestBody() BaseReq<Integer> req){
        return userService.delete(req.getData());
    }

    @ApiOperation(tags = "冻结用户接口", value = "freeze_user")
    @RequestMapping(value = "freeze_user",method = RequestMethod.POST)
    public BaseResp<Integer> freezeUser(@RequestBody() BaseReq<Integer> req){
        return userService.freezeUser(req.getData());
    }

}
